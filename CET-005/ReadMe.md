# CET-005

## VPC Stack:

First a stack with the VPC components is created with 2 public subnets, 2 private subnets, an internet gateway, 2 NAT gateways one for each public subnet with 2 elastic IPs associated with them, and a network ACL 1 for public subnets and 1 for private subnets.

When VPC stack complete its creation it exports the parameters that will be needed in the second stack `servers stack` in the stack export list. I exported vpc id, subnets ids, and vpc CIDR block.

## Servers Stack:

After `VPC stack` created a second stack containing the bastion host and the web servers (public and private) is creted. servers stack contains security gruops (for public and private instances), the bastion, and private servers EC2, and an autoscaling group containing 2 public web servers. The autoscaling group at any time must contain 2 servers as minimum and no more than 4 servers as maximum.

## Parameters:
### VPC Stack

    1- VPC CIDR
    2- Subnets CIDR

### Servers Stack

    1- Bastion host allowed IP

        - Using CIDR block notation eg. 192.168.1.1/32

    2- Key Name to use in creating instances

    3- Instances types (for bastion, private, and public web servers)