# CET-004

## Stack Parameters:

Below are the parameters you need to supply when creating the stack (the template is region independent so it can be created from any region)

1- Instance Name

2- Instance Type

- Default is t2.micro and you can choose from a pre-populated list

3- KeyName

- Choose from an existing keypair from my region

4- Amazon Machine Image Id

- Using the latest ami Id from ssm parameter store

5- S3 Bucket Name

- `mabdelsattar-onica-testbucket1` added from console

6- Html Page prefix

- `index.html` uploaded to the **S3 bucket** from console

7- SubnetId

- Choose from existing subnets in the default VPC


## The Template:

The template creates a security group allowing ssh and http to go in and out from EC2 instance, assign the `AdminAccess` role to the EC2 instance by using the `iamInstanceProfile` property of the EC2 instance (this is to make EC2 instance able to use aws command line tools without manually adding the credentials), and finally uses `cfn-init` helper script to install **apache** and copy the `index.html` from **S3 bucket** to apache root folder to be able to browse it on the internet.
