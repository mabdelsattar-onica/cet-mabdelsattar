# Egypt Team Cloud Engineer Training (CET)

## Purpose: 
You will use this repository to deliver the code for Cloud Engineer training tasks. 
Found here
[Clould engineer training](https://drive.google.com/file/d/15rSV3T2mmZWEbbrQTi9ScNWrX3FsyG65/view?usp=sharing)

## How to use
- Create private Repo forked from https://bitbucket.org/shafik_corpinfo/cet-onica with your name CET-{short_name}.
- Share with the trainers’ bitbucket accounts.
- A daily status should be posted to the `#egypt-training-followup` slack channel with the following format:
```
Y: What you did yesterday, for example, Worked on [CET-xxx] {task-name}
T: What you are doing Today, for example, Continue [CET-xxx] {task-name}
B: If you have any blockers.
```
- Readme file should be written for each task.
- A pull request should be created right after a task is finished.
- The link of the pull request and the actual time (the time you took to complete the task) should be added to the ticket as a comment or posted in the `#egypt-training-followup` slack channel.
- You should demo your tasks in the weekly sprint review.
- After sprint review please make sure to terminate all the resources you have created.

